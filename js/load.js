var Load = function () { };

Load.prototype = {

    init: function () {
        this.loadingBar = game.make.sprite(game.world.centerX - (387 / 2), 400, "loading");
        this.status = game.make.text(game.world.centerX, 380, 'Loading...', { fill: 'white' });
        utils.centerGameObjects([this.status]);
    },

    // The preload function then will call all of the previously defined functions:
    preload: function () {
        game.add.sprite(0, 0, 'stars');
        game.add.existing(this.status);
        game.add.existing(this.loadingBar);
        this.load.setPreloadSprite(this.loadingBar);

        this.loadScripts();
        this.loadImages();
        this.loadFonts();
        this.loadBgm();
    },

    loadScripts: function () {
        game.load.script('menu', 'menu.js');
        game.load.script('game', 'game.js');
        game.load.script('gameOver', 'gameOver.js');
        game.load.script('credits', 'credits.js');
        game.load.script('options', 'options.js');
    },

    loadImages: function () {
        // game.load.image('menu-bg', 'assets/images/menu-bg.jpg');
        // game.load.image('options-bg', 'assets/images/options-bg.jpg');
        // game.load.image('gameover-bg', 'assets/images/gameover-bg.jpg');
        game.load.image('sky', 'assets/images/sky.png');
        game.load.image('ground', 'assets/images/platform.png');
        game.load.image('grass', 'assets/images/grass.png');
        game.load.image('block', 'assets/images/block.png');
        game.load.image('bar', 'assets/images/bar.png');
        game.load.image('ball', 'assets/images/ballblue.png');
        game.load.image('kunai', 'assets/images/kunai.png');
        //Sprites
        game.load.spritesheet('run', 'assets/images/run.png', 363, 450);
        game.load.spritesheet('dead', 'assets/images/dead.png', 482, 450);
        game.load.spritesheet('ninja', 'assets/images/ninja.png', 404, 395);
        game.load.spritesheet('plane', 'assets/images/plane.png', 444, 302);
        game.load.spritesheet('enemy', 'assets/images/ninjaEnemy.png', 404, 395);
    },

    loadFonts: function () {
    },

    loadBgm: function () {
        //Audio
        // game.load.audio('boden', ['assets/sounds/bodenstaendig_2000_in_rock_4bit.mp3', 'assets/sounds/bodenstaendig_2000_in_rock_4bit.ogg']);    
        game.load.audio('megaman', ['assets/sounds/megaman2.mp3', 'assets/sounds/megaman2.ogg']);    
    },


    addGameStates: function () {
        game.state.add("menu", Menu);
        game.state.add("game", Game);
        game.state.add("gameOver", GameOver);
        game.state.add("credits", Credits);
        game.state.add("options", Options);
    },

    addGameMusic: function () {
        musicPlayer = game.add.audio('megaman');
        musicPlayer.loop = true;
        musicPlayer.volume = 0.5;
        musicPlayer.play();
    },

    create: function () {
        this.status.setText('Ready!');
        this.addGameStates();
        this.addGameMusic();

        setTimeout(function () {
            game.state.start("menu");
        }, 1000);
    }
}
