var Game = function () { };

Game.prototype = {

    preload: function () {

    },

    create: function () {
        timer = game.time.create(false);
        this.initGraphics();
        this.initPhysics();
        this.initSounds();
        cursors = game.input.keyboard.createCursorKeys();
        // pauseKey = this.input.keyboard.addKey(Phaser.Keyboard.P);
        // game.input.onDown.add(this.unpause, self);
        shootKey = game.input.mousePointer.leftButton;
        slideKey = game.input.mousePointer.rightButton;
        jumpKey = game.input.keyboard.addKey(Phaser.KeyCode.SPACEBAR);
        game.canvas.oncontextmenu = function (e) { e.preventDefault(); }
        this.resetGame();
    },

    initGraphics: function () {
        game.add.sprite(0, 0, 'sky');
        ground = game.add.tileSprite(0, game.world.height - 50, game.world.width, game.world.width, 'grass');
        //player
        player = game.add.sprite(120, game.world.height - 100, 'ninja');
        player.animations.add('dead', [10, 11, 12, 13, 14, 15, 16, 17, 18, 19], 15, false);
        player.animations.add('idle', [30, 31, 32, 33, 34, 35, 36, 37, 38, 39], 10, true);
        player.animations.add('jump', [40, 41, 42, 43, 44, 45, 46, 47, 48, 49], 10, false);
        player.animations.add('run', [70, 71, 72, 73, 74, 75, 76, 77, 78, 79], 25, true);
        player.animations.add('slide', [80, 81, 82, 83, 84, 85, 86, 87, 88, 89], 10, false);
        player.animations.add('throw', [90, 91, 92, 93, 94, 95, 96, 97, 98, 99], 10, false);
        player.scale.setTo(0.15, 0.15);
        player.anchor.setTo(0.2, 0.45);
        PlayerIsRunning = false;
        player.animations.play('idle');
        //powerBar
        powerBar = game.add.sprite(player.x + 25, player.y - 15, 'bar');
        powerBar.width = 0;
        //block
        blocks = game.add.group();
        //plane
        plane = game.add.sprite(0, 0, 'plane');
        plane.animations.add('fly', [0, 1], 25, true);
        //enemy
        enemy = game.add.sprite(game.world.width + 350, game.world.height - 100, 'enemy');
        enemy.animations.add('attack', [0, 1, 2, 3, 4, 5, 6, 7, 8, 9], 10, true);
        enemy.animations.add('dead', [10, 11, 12, 13, 14, 15, 16, 17, 18, 19], 15, false);
        enemy.animations.add('idle', [30, 31, 32, 33, 34, 35, 36, 37, 38, 39], 10, true);
        enemy.animations.play('idle');
        enemy.scale.setTo(0.15, 0.15);
        enemy.anchor.setTo(0.2, 0.08);
        enemyIsDead = false;
        //weapon
        weapon = game.add.weapon(1, 'kunai');
        weapon.bulletKillType = Phaser.Weapon.KILL_WORLD_BOUNDS;
        weapon.bulletAngleOffset = 90;
        weapon.bulletSpeed = 400;
        weapon.trackSprite(player, 25, 0);
        weapon.bullets.setAll('scale.x', 0.2);
        weapon.bullets.setAll('scale.y', 0.2);
        weapon.fireAngle = 0;
        //instructions
        game.add.text(16, 16, "Evitez les blocks, les ninjas et les avions!", { font: '12px Arial', fill: '#ff0' });
        game.add.text(16, 32, "Espace pour sauter ( rester appuyé pour sauter plus haut).", { font: '12px Arial', fill: '#ff0' });
        game.add.text(16, 48, "Clic-gauche: lancer un kunai (tue les ninjas) / Clic-droit: effectuer un slide (réduit légérement la hitbox).", { font: '12px Arial', fill: '#ff0' });
        game.add.text(16, 64, "En mode facile , vous pouvez tomber sur les blocks et les avions", { font: '12px Arial', fill: '#ff0' });
        //timer
        timerText = game.add.text(gameOptions.screenWidth * 0.5, gameOptions.screenHeight * 0.35, "0", { font: '62px Arial', fill: '#ff0' });
        timerText.anchor.set(0.5, 0);
        //score

        score = 0;
        scoreText = game.add.text(gameOptions.screenWidth * 0.8, 0, "Score: " + score, { font: '20px Arial', fill: '#ff0' });
        scoreText.anchor.set(0.5, 0);
    },

    initPhysics: function () {
        // physics
        game.physics.startSystem(Phaser.Physics.ARCADE);
        game.physics.arcade.enable(ground);
        game.physics.arcade.enable(player);
        game.physics.arcade.enable(enemy);
        ground.enableBody = true;
        ground.body.immovable = true;
        player.body.gravity.y = 200;
        game.physics.arcade.enable(blocks);
        player.body.setSize(280, 350, 0, 0);
    },

    initSounds: function () {
        // sounds
    },

    resetGame: function () {
        timer.loop(Phaser.Timer.SECOND * 3.5, this.startGame, this);
        timer.start();
        blocks.removeAll();
        plane.destroy();
        jumpKey.onDown.remove(this.chargeJump, this);
        shootKey.onDown.remove(this.shootKunai, this);
        slideKey.onDown.remove(this.slide, this);
        PlayerIsRunning = false;
        PlayerIsDead = false;
        player.animations.play('idle');
    },

    startGame: function () {
        timer.stop(false);
        ground.autoScroll(-150, 0);
        this.makeBlocks();
        this.makePlane();
        this.makeEnemy();
        game.physics.arcade.enable(block);
        jumpKey.onDown.add(this.chargeJump, this);
        shootKey.onDown.add(this.shootKunai, this);
        slideKey.onDown.add(this.slide, this);
        PlayerIsRunning = true;
        player.animations.play('run');
    },

    update: function () {
        game.physics.arcade.collide(player, ground);
        game.physics.arcade.collide(blocks, ground);
        game.physics.arcade.collide(blocks);
        if (blocks.length > 0) {
            fchild = blocks.getChildAt(0);
            if (fchild.x < -50) {
                this.makeBlocks();
                score += 50;
                this.updateScoreText();
            }
        }
        if (plane.x < -120) {
            this.makePlane();
            plane.animations.play('fly');
            score += 50;
            this.updateScoreText();
        }

        if (enemy.x < -120) {
            this.makeEnemy();
            enemy.animations.play('idle');
            score += 25;
            this.updateScoreText();
        }
        game.physics.arcade.collide(player, blocks, this.killedByBlocks, null, this);
        game.physics.arcade.collide(player, plane, this.killedByPlane, null, this);
        if (!PlayerIsDead) {
            if (player.body.touching.down) {
                canJump = true;
                if (PlayerIsRunning) { player.animations.play('run'); }
            } else {
                canJump = false;
            }
        };

        if (!enemyIsDead) {
            game.physics.arcade.overlap(player, enemy, this.killedByWorld, null, this);
            game.physics.arcade.overlap(enemy, weapon.bullets, this.enemyKilled, null, this);
            if (enemy.x == player.x + 120) {
                enemy.animations.play('attack');
                enemy.body.setSize(280, 350, 0, 0);
            }
        };


        timerText.text = '';
        if (timer.running) {
            timerText.alpha = 1;
            if (timer.duration / 1000 < 0.5) {
                timerText.text = 'GO!';
            } else {
                timerText.text = Math.round(timer.duration / 1000);
            }
            game.add.tween(timerText).to({ alpha: 0.1 }, 500, Phaser.Easing.None, true);
        };

        if (player.x < 0) { this.killedByWorld(); }
    },

    shootKunai: function () {
        if (!PlayerIsDead) {
            // player.animations.play('throw');
            game.time.events.add(Phaser.Timer.SECOND / 10, this.delayShoot, this);
        }
    },

    enemyKilled: function () {
        if (!enemyIsDead) {
            enemyIsDead = true;
            enemy.animations.play('dead');
            score += 25;
            this.updateScoreText();
        }
    },

    slide: function () {
        if (!PlayerIsDead) {
            PlayerIsRunning = false;
            player.body.setSize(280, 250, 0, 100);
            player.animations.play('slide');
            game.time.events.add(Phaser.Timer.SECOND, this.delayIsRunning, this);
        }
    },

    delayIsRunning: function () {
        PlayerIsRunning = true;
        player.body.setSize(280, 350, 0, 0);
    },

    updateScoreText: function () {
        scoreText.text = 'Score: ' + score;
    },


    delayShoot: function () {
        weapon.fire();
    },

    render: function () {
        // game.debug.text(enemy.x, 16, 16);
        // game.debug.text(fchild.x, 16, 32);
        // game.debug.bodyInfo(player, 16, 48);
        // game.debug.body(player);
        // game.debug.body(enemy);
        // game.debug.body(plane);
        // weapon.debug();
    },

    chargeJump: function () {
        if (!PlayerIsDead) {
            if (canJump) {
                jumpKey.onDown.remove(this.chargeJump, this);
                timer = game.time.events.loop(Phaser.Timer.SECOND / 1000, this.increasePower, this);
                jumpKey.onUp.add(this.jump, this)
            }
        }
    },

    jump: function () {
        PlayerIsRunning = false;
        jumpKey.onUp.remove(this.jump, this);
        this.doJump();
        game.time.events.remove(timer);
        power = 0;
        powerBar.width = 0;
        jumpKey.onDown.add(this.chargeJump, this);
        PlayerIsRunning = true;
    },

    increasePower: function () {
        power++;
        powerBar.height = 10;
        powerBar.width = power * 2;
        if (power > 45) {
            power = 45;
        }
    },

    doJump: function () {
        player.animations.play('jump');
        player.body.velocity.y = power * -10;
    },

    makeBlocks: function () {
        blocks.removeAll();
        wallHeight = game.rnd.integerInRange(1, 5);
        for (i = 0; i < wallHeight; i++) {
            block = game.add.sprite(game.world.width, (game.world.height - 100) - i * 50, 'block');
            blocks.add(block);
        }
        blocks.forEach(function (block) {
            game.physics.enable(block, Phaser.Physics.ARCADE);
            block.body.velocity.x = -150;
            block.body.gravity.y = 400;
            block.body.bounce.set(0.5, 0.5);
        });
    },

    makePlane: function () {
        if (plane) {
            plane.destroy();
        }
        planeY = game.rnd.integerInRange(game.height * .1, game.height * .4);
        plane = game.add.sprite(game.width + 100, planeY, 'plane');
        plane.animations.add('fly', [0, 1], 25, true);
        game.physics.enable(plane, Phaser.Physics.ARCADE);
        plane.body.velocity.x = -200;
        plane.body.bounce.set(2, 2);
        plane.scale.setTo(0.2, 0.2);
        ground.body.immovable = true;
        plane.body.setSize(400, 245, 0, 20);
    },

    makeEnemy: function () {
        console.log('makeEnemy');
        if (enemy) {
            enemy.destroy();
        }
        // planeY = game.rnd.integerInRange(game.height * .1, game.height * .4);
        enemy = game.add.sprite(game.world.width + 350, game.world.height - 100, 'enemy');
        enemy.animations.add('attack', [0, 1, 2, 3, 4, 5, 6, 7, 8, 9], 10, true);
        enemy.animations.add('dead', [10, 11, 12, 13, 14, 15, 16, 17, 18, 19], 15, false);
        enemy.animations.add('idle', [30, 31, 32, 33, 34, 35, 36, 37, 38, 39], 10, true);
        enemy.animations.play('idle');
        game.physics.enable(enemy, Phaser.Physics.ARCADE);
        enemy.body.velocity.x = -150;
        enemy.body.bounce.set(2, 2);
        enemy.scale.setTo(0.15, 0.15);
        enemy.anchor.setTo(0.2, 0.08);
        enemy.body.setSize(200, 350, 180, 50);
        enemyIsDead = false;
    },

    delayOver: function () {
        if (!PlayerIsDead) {
            PlayerIsDead = true;
            player.body.setSize(320, 200, 0, 150);
            player.animations.play('dead');
            game.time.events.add(Phaser.Timer.SECOND * 2, this.gameOver, this);
        }
    },

    killedByBlocks: function () {

        if (gameOptions.normalMode) {
            this.delayOver();
        } else {
            if (!player.body.touching.down) {
                this.delayOver();
            }
        }
    },

    killedByPlane: function () {
        if (gameOptions.normalMode) {
            this.delayOver();
        } else {
            if (!player.body.touching.down) {
                this.delayOver();
            }
        }
    },

    killedByWorld: function () {
        this.delayOver();
    },

    gameOver: function () {
        game.state.start("gameOver");
    },

    setPause: function () {
        game.paused = true;
        console.log('pause');
    },

    unpause: function (event) {
        if (game.paused) {
            console.log('unpause');
            game.paused = false;
        }
    },
}    